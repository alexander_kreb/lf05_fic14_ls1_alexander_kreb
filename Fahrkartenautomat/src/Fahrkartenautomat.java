﻿import java.util.Scanner;
	
class Fahrkartenautomat
{ 
	static Scanner tastatur = new Scanner(System.in);
    
	public static void main(String[] args)
    {
    	int prüfnummer =1;
		while (prüfnummer  > 0) {
    	
      
      
       double zuZahlenderBetrag; 
      
       double rückgabebetrag;
      
       
       	zuZahlenderBetrag = fahrkartenbestellungErfassen();
    	rückgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);
    	fahrkartenAusgeben();
    	rueckgeldAusgeben(rückgabebetrag);
    	prüfnummer = mehr();
		}
    	System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                "vor Fahrtantritt entwerten zu lassen!\n"+
                "Wir wünschen Ihnen eine gute Fahrt.");
    	
    }
    	
       public static double fahrkartenbestellungErfassen () {
    	   
  
     
    	   String[] tickets = { "Einzelfahrschein Berlin AB", "Einzelfahrschein Berlin BC", "Einzelfahrschein Berlin ABC","Kurzstrecke","Tageskarte Berlin AB","Tageskarte Berlin BC","Tageskarte Berlin ABC","Kleingruppen-Tageskarte Berlin AB","Kleingruppen-Tageskarte Berlin BC","Kleingruppen-Tageskarte Berlin ABC" };   	   
    	   
    	   double[] preise =  { 2.90,3.30,3.60,1.90,8.60,9.00,9.60,23.50,24.50,24.90 };
    	   
    	   
    	   for (int i = 0; i <tickets.length; i++) {
    		   System.out.printf( i+1 + "%35s %20.2f € \n" ,tickets[i],preise[i]);
    	   }
    	   
    	   
    	   double zuZahlenderBetrag = 0;
    	   int ticket = 0;
    	   System.out.println("\nWelches Ticket möchten Sie erwerben? Geben Sie die Ticketnummer ein");
    	   
    	   
    	   int x = tastatur.nextInt();
    	   
    	   zuZahlenderBetrag =  (preise[x-1]);
    	   
    	   
    	   
    	  
    	   
    	   
    	   
    	 
       System.out.print("Anzahl der tickets: ");
       double ticketAnzahl = tastatur.nextInt();
       
       if (ticketAnzahl > 10) {
    	   ticketAnzahl = 1;
    	   System.out.println("Ungültige Menge!!! (Sie können nicht mehr als 10 Karten aufeinmal kaufen) \nStandartwert wird angewandt (1).");
       }
       
       zuZahlenderBetrag *= ticketAnzahl ;
       return zuZahlenderBetrag;
    	}
       // Geldeinwurf
       // -----------
       public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
    	   
 
       double eingezahlterGesamtbetrag = 0.0;
       while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
       {
    	  
    	   System.out.printf("Noch zu zahlen: %.2f Euro \n",(zuZahlenderBetrag - eingezahlterGesamtbetrag));
    	  
    	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
    	  double eingeworfeneMünze = tastatur.nextDouble();
           eingezahlterGesamtbetrag += eingeworfeneMünze;
       }
       double rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
       return rückgabebetrag;
       }

       // Fahrscheinausgabe
       // -----------------
       public static void fahrkartenAusgeben() {
      
     
    	   System.out.println("\nFahrschein(e) wird/werden ausgegeben");
       
    	   
       
       for (int i = 0; i < 8; i++)
       {
          System.out.print("=");
          try {
			Thread.sleep(250);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
       }
       System.out.println("\n\n");
       }
       // Rückgeldberechnung und -Ausgabe
       // -------------------------------
       public static void rueckgeldAusgeben(double rückgabebetrag) {
    
       if(rückgabebetrag > 0.0)
       {
    	   System.out.printf("Der Rückgabebetrag in Höhe von %.2f EURO ",rückgabebetrag);
    	   System.out.println("wird in folgenden Münzen ausgezahlt:");

           while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
           {
        	  System.out.println("2 EURO");
	          rückgabebetrag -= 2.0;
           }
           while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
           {
        	  System.out.println("1 EURO");
	          rückgabebetrag -= 1.0;
           }
           while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
           {
        	  System.out.println("50 CENT");
	          rückgabebetrag -= 0.5;
           }
           while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
           {
        	  System.out.println("20 CENT");
 	          rückgabebetrag -= 0.2;
           }
           while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
           {
        	  System.out.println("10 CENT");
	          rückgabebetrag -= 0.1;
           }
           while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
           {
        	  System.out.println("5 CENT");
 	          rückgabebetrag -= 0.05;
           }
       }
       
      
       	
       		 
      
    }
       public static int mehr() {
      		System.out.println("Möchten Sie weitere Fahrkarten kaufen? Ja = 1 , Nein = 0");
      		 int prüfnummer = tastatur.nextInt();
      		 return prüfnummer;
       }
}


		// Aufgabe 2.5
		// Nr. 5 : Ich habe den Datentyp Int gewählt, da immer nur ganze Karten verkauft werden können.(ganze Zahlen reichen aus)
		// Nr. 6 : Bei "zuZahlenderBetrag *= ticketAnzahl ;" in Zeile 21 werden die 2 Zahlen, die vorher mittels Scanner bestimmt worden 
		// 			miteinander multipliziert.