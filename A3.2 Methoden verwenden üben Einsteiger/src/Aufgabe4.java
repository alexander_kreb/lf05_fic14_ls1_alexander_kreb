import java.util.Scanner;
import java.lang.Math;
public class Aufgabe4 {
	
	static Scanner myScanner = new Scanner(System.in);

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		
		System.out.println(" W�hlen Sie Nummer des zu berechnenden K�rpers ( 1: W�rfel, 2: Quader, 3: Pyramide, 4: Kugel )");
		
		int k�rper = myScanner.nextInt();
		
		
		
		if 		(k�rper == 1) 	{
			
			System.out.println("Das Volumen betr�gt: " + volumenW�rfel() );
				
		}
		
		else if (k�rper == 2) 	{
			
			System.out.println("Das Volumen betr�gt: " + volumenQuader() );
			
		}
		
		else if (k�rper == 3) 	{
			
			System.out.println("Das Volumen betr�gt: " + volumenPyramide() );
			
		}

		else if (k�rper == 4) 	{

			System.out.println("Das Volumen betr�gt: " + volumenKugel() );
			
		}
		
		else {
			
			System.out.println("Bitte w�hlen Sie einen g�ltigen K�rper aus! (1: W�rfel, 2: Quader, 3: Pyramide, 4: Kugel)");
			
		}
		
		
		
	}

	public static double volumenKugel() {
		
		System.out.println("Kugel wurde ausgew�hlt (V = 4/3 * r� * Pi)");
		
		System.out.println("Radius r: ");
		double radius = myScanner.nextInt();
		
		double ergebnis = 4/3 * Math.pow(radius, 4.0) * Math.PI ;
		return ergebnis;
	}

	private static double volumenPyramide() {

		System.out.println("Pyramide wurde ausgew�hlt (V = a * a * h / 3)");
		
		System.out.println("Kantenl�nge a: ");
		double kanteA = myScanner.nextInt();
		
		System.out.println("H�he h: ");
		double h�he = myScanner.nextInt();
		
		double ergebnis = kanteA * kanteA * h�he / 3;
		return ergebnis;
	}

	private static double volumenQuader() {

		System.out.println("Quader wurde ausgew�hlt (V = a * b * c)");

		System.out.println("Kantenl�nge a: ");
		double kanteA = myScanner.nextInt();
		
		System.out.println("Kantenl�nge b: ");
		double kanteB = myScanner.nextInt();
		
		System.out.println("Kantenl�nge c: ");
		double kanteC = myScanner.nextInt();
		
		double ergebnis = kanteA * kanteB *kanteC;
		return ergebnis;
	}

	private static double volumenW�rfel() {

		System.out.println("W�rfel wurde ausgew�hlt (V = a * a * a)");

		System.out.println("Kantenl�nge a: ");
		double kanteA = myScanner.nextInt();
		
		double ergebnis = kanteA * 3 ;
		return ergebnis;
	}

}