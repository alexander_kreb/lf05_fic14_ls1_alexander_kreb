import java.util. Scanner;

public class Aufgabe2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner myScanner = new Scanner(System.in);
		
		System.out.print("Bitte geben Sie Ihren Namen an: ");
		String name = myScanner.next();
		
		System.out.print("Bitte geben Sie ihr Alter an: ");
		int alter = myScanner.nextInt();
		
		System.out.println("Sie heissen "+name+" und sind "+alter+" Jahre alt.");
		
		if (alter >= 18) {
			System.out.println("Sie sind Volljaehrig.");
		}
		else{
			System.out.println("Sie sind nicht Volljaehrig.");
		}
		
	}

}
