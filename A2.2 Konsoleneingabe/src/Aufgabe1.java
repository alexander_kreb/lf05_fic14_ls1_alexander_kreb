import java.util.Scanner; // Import der Klasse Scanner

public class Aufgabe1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		// Neues Scanner-Objekt myScanner wird erstellt
		Scanner myScanner = new Scanner(System.in);
		
		//Operator wird abgefragt.
		System.out.println("Bitte geben Sie einen Rechenoperator ein (+,-,*,/)");
		char operator = myScanner.next().charAt(0);
		
		System.out.print("Bitte geben Sie eine Zahl ein: ");
		double zahl1 = myScanner.nextDouble();
		
		System.out.print("Bitte geben Sie eine zweite Zahl ein: ");
		double zahl2 =myScanner.nextDouble();
		
		double ergebnis = 0.0;
		
		if (operator == '+') {
			ergebnis = zahl1 + zahl2;
		}
		else if (operator == '-') {
			ergebnis = zahl1 - zahl2;
		}
		else if (operator == '*') {
			ergebnis = zahl1 * zahl2;
		}
		else if (operator == '/') {
			ergebnis = zahl1 / zahl2;
		}
		else {
			System.out.println("!!!!UNGUELTIGE ANGABE!!!!");
		}
		
		System.out.print("Das Ergebnis der Rechnung lautet: ");
		System.out.print(ergebnis);
		
		myScanner.close();

	}

}
