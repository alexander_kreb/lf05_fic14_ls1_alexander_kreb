
public class Aufgabe2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String a ;
		String b ;
		String c ;
		
		a = "0!";
		b = "=";
		c = "1";
		System.out.printf("%-5s%-19s=%4s\n", a, b, c);
		
		a = "1!";
		b = "= 1 *";
		c = "1";
		System.out.printf("%-5s%-19s=%4s\n", a, b, c);
		
		a = "2!";
		b = "= 1 * 2 *";
		c = "2";
		System.out.printf("%-5s%-19s=%4s\n", a, b, c);
		
		a = "3!";
		b = "= 1 * 2 * 3";
		c = "6";
		System.out.printf("%-5s%-19s=%4s\n", a, b, c);
		
		a = "4!";
		b = "= 1 * 2 * 3 * 4";
		c = "24";
		System.out.printf("%-5s%-19s=%4s\n", a, b, c);
		
		a = "5!";
		b = "= 1 * 2 * 3 * 4 * 5";
		c = "120";
		System.out.printf("%-5s%-19s=%4s\n", a, b, c);
		
	}

}
